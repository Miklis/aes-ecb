#ifndef AEC_ECB_RETURN_CODE_H
#define AEC_ECB_RETURN_CODE_H

typedef enum {
    OK                          = 0,
    MISSING_ARGUMENT            = 1,
    FATAL_ERROR                 = 2,
    NON_EXISTENT_SOLUTION       = 3,
    WRONG_KEY_LENGTH            = 4,
    DATA_READ_ERROR             = 5
}return_code;

#endif
