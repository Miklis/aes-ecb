#include <stdlib.h>
#include <stdio.h>
#include "return_code.h"

/*
   Pomocná funkce pro alokování 2d pole.

   Vrací return code:
   OK (0) - vše proběhlo v pořádku
   FATAL_ERROR (2) - chyba při manipulaci s pamětí
 */
return_code alloc_matrix_2d(unsigned char ***out_matrix, unsigned width_and_height){
    unsigned char **matrix = NULL;
    int i;

    matrix = (unsigned char **) calloc(width_and_height, sizeof(unsigned char *));

    if(matrix == NULL){
        return FATAL_ERROR;
    }

    for(i = 0; i < width_and_height; i++){
        matrix[i] = (unsigned char *)calloc(width_and_height, sizeof(unsigned char));
        if(matrix[i] == NULL){
            return FATAL_ERROR;
        }
    }

    *out_matrix = matrix;
    return OK;
}

/*
    Pomocná funkce pro alokování 2d pole.

    Vrací return code:
    OK (0) - vše proběhlo v pořádku
    FATAL_ERROR (2) - chyba při manipulaci s pamětí
 */
return_code alloc_matrix_3d(unsigned char ****out_matrix, unsigned width_and_height, unsigned depth){
    unsigned char ***matrix = NULL;
    int i;

    return_code code;
    matrix = (unsigned char ***)malloc(depth  * sizeof(unsigned char **));

    if(matrix == NULL){
        return FATAL_ERROR;
    }

    for(i = 0; i < depth; i++){
         code = alloc_matrix_2d(&(matrix[i]), width_and_height);
         if(code == FATAL_ERROR){
             return code;
         }
    }

    *out_matrix = matrix;
    return OK;
}

/*
  Uvolní paměť předaného 2d pole.
 */
void free_matrix_2d(unsigned char ***to_free_matrix, unsigned length){
    int i;
    unsigned char **matrix = *to_free_matrix;

    for(i = 0; i < length; i++){
        free(matrix[i]);
    }

    free(matrix);
    *to_free_matrix = NULL;
}

/*
  Uvolní paměť předaného 3d pole.
 */
void free_matrix_3d(unsigned char ****to_free_matrix, unsigned length, unsigned depth){
    int i, j;
    unsigned char ***matrix = *to_free_matrix;

    for(i = 0; i < depth; i++){
        for(j = 0; j < length; j++){
            free(matrix[i][j]);
        }
        free(matrix[i]);
    }

    free(matrix);

    *to_free_matrix = NULL;
}

/*
  Motoda zajistí naplnění dat do předaného 2d pole z 1d pole.
 */
void fill_matrix(unsigned char **matrix, char *data, unsigned length){
    int i,j;
    for(i = 0; i < length; i++){
        for(j = 0; j < length; j++){
            matrix[j][i] = data[i * length + j];
        }
    }
}


/*
  Vytiskne předané 2d pole charů do řádku, kdy tiskne znaky v hexadecimálním
  formátu.
 */
void print_matrix_2d_as_line(unsigned char **matrix, unsigned length){
    int i, j;

    for(i = 0; i < length;i++){
        for(j = 0; j < length;j++){
            printf("%02x ", matrix[j][i]);
        }
    }
    printf("\n");
}


void print_matrix_2d(unsigned char **matrix, unsigned length){
    int i, j;

    for(i = 0; i < length;i++){
        for(j = 0; j < length;j++){
            printf("%02x ", matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void print_matrix_3d(unsigned char ***matrix, unsigned width_and_length, unsigned depth){
    int i, j, k;

    for(i = 0; i < depth; i++){
        for(j = 0; j < width_and_length; j++){
            for(k = 0; k < width_and_length; k++){
                printf("%02x ", matrix[i][j][k]);
            }
            printf("\n");
        }
        printf("\n");
        printf("\n");
    }
}
