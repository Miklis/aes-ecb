
#ifndef AEC_ECB_ENCRYPT_H
#define AEC_ECB_ENCRYPT_H
#include "return_code.h"

#define AES_128_KEY_LENGTH 16

return_code encrypt(FILE *fp, char *key);

#endif
