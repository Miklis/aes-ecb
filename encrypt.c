#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "return_code.h"
#include "matrix_lib.h"
#include "encrypt.h"

#define NUMBER_OF_ROUNDED_KEYS 11
#define BUFFER_SIZE 16
#define MATRIX_LENGTH 4

/*
  Rijndael S-box table
 */
static const unsigned char s_box[16][16] = {
        /*0     1      2     3     4     5     6     7     8     9     a     b     c     d     e     f */
     /*0*/{0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76},
     /*1*/{0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0},
     /*2*/{0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15},
     /*3*/{0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75},
     /*4*/{0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84},
     /*5*/{0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf},
     /*6*/{0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8},
     /*7*/{0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2},
     /*8*/{0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73},
     /*9*/{0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb},
     /*a*/{0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79},
     /*b*/{0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08},
     /*c*/{0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a},
     /*d*/{0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e},
     /*e*/{0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf},
     /*f*/{0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16},
};

/*
  Rcon konstanty
 */
static const unsigned char rcon[NUMBER_OF_ROUNDED_KEYS] = {
        0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36
};

/*
  Tabulka pro operaci prohazování sloupců
 */
static const unsigned char rgf[4][4] = {
        {0x02, 0x03, 0x01, 0x01},
        {0x01, 0x02, 0x03, 0x01},
        {0x01, 0x01, 0x02, 0x03},
        {0x03, 0x01, 0x01, 0x02},
};

/*
    Funkce přijímá 8bitovou informaci na jejíž základě
    se získá záměněný byte z s_boxu.
    Číslo řádku v s_boxu se získá z prvních 4 bitů předané informace
    a sloupec dalších 4 bitů.

    Funkce pak vrací výsledný zaměněný byte.
 */
unsigned char get_sub_byte(unsigned char m_pos){
    unsigned char row, column, res;

    row = (unsigned)((m_pos & 0xf0) >> 4);
    column = (unsigned)(m_pos & 0x0f);

    res = s_box[row][column];

    return res;
}

/*
    Pro každý index v 2d poli provede operaci
    záměny bytů z Rijndealova s_boxu.

    Funkce přijímá parametr a to 2d pole charů
    do které se provádí změny
 */
void sub_bytes(unsigned char **matrix, int length){
    int i, j;

    for(i = 0; i < length; i++){
        for(j = 0; j < length; j++){
            matrix[i][j] = get_sub_byte(matrix[i][j]);
        }
    }
}

/*
    Posune řádky předená matice o to tak, že prvni řádek posune
    o nula pozic, druhý posune data o jednu pozici vlevo,
    třetí o dvě pozice vlevo a poslední o tři pozice doleva.
 */
void shift_rows(unsigned char **matrix){
    unsigned char temp;

    temp = matrix[1][0];
    matrix[1][0] = matrix[1][1];
    matrix[1][1] = matrix[1][2];
    matrix[1][2] = matrix[1][3];
    matrix[1][3] = temp;

    temp = matrix[2][0];
    matrix[2][0] = matrix[2][2];
    matrix[2][2] = temp;

    temp = matrix[2][1];
    matrix[2][1] = matrix[2][3];
    matrix[2][3] = temp;


    temp = matrix[3][0];
    matrix[3][0] = matrix[3][3];
    matrix[3][3] = matrix[3][2];
    matrix[3][2] = matrix[3][1];
    matrix[3][1] = temp;
}

/*
  Funkce vytváří jednotlivé podlíče (též známé jako rundovní klíče).
  Proces probíhá, tak že se vezme poslední sloupec z matice key a
  ten posune data o jedna nahoru. Poté se provede na sloupci operace prohození bytů
  a nakonec se logicky sečte s prvním sloupcem z matice key a rcon pole.
  Zbylé tři sloupce se získají logickým součtem i-tého sloupce z key matice
  a i-1 sloupce z matice out_rounded_key, pro i začínající i=1.

  Vstupem do funkce je matice key ze které se bude odpovozat nasledující podklíč
  rcon_index hodnota určující jaká konstanta z rcon indexu se má použít.

  Výstup se vrací přes pointer na pointer out_rounded_key
 */
void expand_key(unsigned char **key, unsigned char **out_rounded_key, int matrix_length, int rcon_index){
    int i, j;

    out_rounded_key[0][0] = key[0][0] ^ get_sub_byte(key[1][3]) ^ rcon[rcon_index];
    out_rounded_key[1][0] = key[1][0] ^ get_sub_byte(key[2][3]) ^ 0x00;
    out_rounded_key[2][0] = key[2][0] ^ get_sub_byte(key[3][3]) ^ 0x00;
    out_rounded_key[3][0] = key[3][0] ^ get_sub_byte(key[0][3]) ^ 0x00;

    for(i = 1; i < matrix_length; i++){ /*column*/
        for(j = 0; j < matrix_length; j++){ /*row*/
            out_rounded_key[j][i] = key[j][i] ^ out_rounded_key[j][i - 1];
        }
    }


}


/*
   Funkce vytvoří podlíče pro jednolivé fáze šifrování.

   Funkce přijímá jako parametry out_expanded_key čež je pointer na 3d pole
   do něhož se uloží výsledné podklíče a key který se bere jako základ pro vytvoření
   podklíčů.

   Vrací return code:
   OK (0) - vše proběhlo v pořádku
   FATAL_ERROR (2) - chyba při manipulaci s pamětí

 */
return_code key_expansion(unsigned char ****out_expanded_key, char *key, unsigned lenght){
    int i, j;
    unsigned char ***expanded_key;
    unsigned char **key_in_matrix = NULL;

     if(alloc_matrix_3d(&expanded_key, lenght, NUMBER_OF_ROUNDED_KEYS) == FATAL_ERROR){
        return FATAL_ERROR;
    }

    if(FATAL_ERROR == alloc_matrix_2d(&key_in_matrix, lenght)){
        return FATAL_ERROR;
    }

    fill_matrix(key_in_matrix, key, lenght);

     for(i = 0; i < lenght;i++){
         for(j = 0; j < lenght;j++){
             expanded_key[0][i][j] = key_in_matrix[i][j];
         }
     }

    expand_key(expanded_key[0], expanded_key[1], lenght, 0);

    for(i = 1; i < NUMBER_OF_ROUNDED_KEYS - 1; i++){
        expand_key(expanded_key[i], expanded_key[i + 1], lenght, i);
    }

    free_matrix_2d(&key_in_matrix, lenght);
    *out_expanded_key = expanded_key;

    return OK;

}

/*
    Funkce vykoná logický and mezi daty a klíčem a výstup
    operace se ukládá do datové matice.
*/
void add_rounded_key(unsigned char **data, unsigned char **key, int lenght){
    int i, j;

    for(i = 0; i < lenght; i++){
        for(j = 0; j < lenght; j++){
            data[i][j] = data[i][j] ^ key[i][j];
        }
    }
}

/*
  Funkce provádí násobení v aritmetice modulo
 */
unsigned char gmul(unsigned char a, unsigned char b) {
    unsigned char p = 0; /* the product of the multiplication */
    while (a && b) {
        if (b & 1) {/* if b is odd, then add the corresponding a to p (final product = sum of all a's corresponding to odd b's) */
            p ^= a; /* since we're in GF(2^m), addition is an XOR */
        }
        if (a & 0x80) {/* GF modulo: if a >= 128, then it will overflow when shifted left, so reduce */
            a = (a << 1) ^
                0x11b; /* XOR with the primitive polynomial x^8 + x^4 + x^3 + x + 1 (0b1_0001_1011) – you can change it but it must be irreducible */
        }else {
            a <<= 1; /* equivalent to a*2 */
        }
        b >>= 1; /* equivalent to b // 2 */
    }
    return p;
}


/*
    Funkce obstarává kombinování sloupů kdy do funkce vstupuje matice data
    a postupně se všechny sloupce této matice násobí v aritmetice modulo s maticí rgf
 */
void mix_columns(unsigned char **data, unsigned matrix_size){
    int i;
    unsigned char temp_1, temp_2, temp_3, temp_4;

    for(i = 0; i < matrix_size; i++){
        temp_1 = gmul(rgf[0][0], data[0][i]) ^ gmul(rgf[0][1], data[1][i]) ^ gmul(rgf[0][2], data[2][i]) ^ gmul(rgf[0][3], data[3][i]);
        temp_2 = gmul(rgf[1][0], data[0][i]) ^ gmul(rgf[1][1], data[1][i]) ^ gmul(rgf[1][2], data[2][i]) ^ gmul(rgf[1][3], data[3][i]);
        temp_3 = gmul(rgf[2][0], data[0][i]) ^ gmul(rgf[2][1], data[1][i]) ^ gmul(rgf[2][2], data[2][i]) ^ gmul(rgf[2][3], data[3][i]);
        temp_4 = gmul(rgf[3][0], data[0][i]) ^ gmul(rgf[3][1], data[1][i]) ^ gmul(rgf[3][2], data[2][i]) ^ gmul(rgf[3][3], data[3][i]);

        data[0][i] = temp_1;
        data[1][i] = temp_2;
        data[2][i] = temp_3;
        data[3][i] = temp_4;
    }


}

/*
   funkce provádí postupně kroky aes algoritmu tedy:
        - Přidání prvního podklíče
        - Pro klíč délky 128 bit opakuje 10x tyto kroky:
            * Záměna bytů podle s_box tabulky
            * Prohození řádků
            * Kombinování sloupců
            * Přidání klíče
        - Záměna bytů podle s_box tabulky
        - Prohození řádků
        - Přidání podklíče

    Vstupem metody jsou data v 2d poli a jednotlivé podlíče roprezentovány 3d polem
    Výsledkem jsou zašifrovaná data v parametru data
 */
void encrypt_data(unsigned char **data, unsigned char ***key, unsigned matrix_size){
    int i;
    add_rounded_key(data, key[0], matrix_size);
    for(i = 1; i < NUMBER_OF_ROUNDED_KEYS - 1; i++){
       sub_bytes(data, matrix_size);
       shift_rows(data);
       mix_columns(data, matrix_size);
       add_rounded_key(data, key[i], matrix_size);
    }
    sub_bytes(data, matrix_size);
    shift_rows(data);
    add_rounded_key(data, key[NUMBER_OF_ROUNDED_KEYS - 1], matrix_size);
}


/*
     Hlavní funkce pro šifrování textu, která provádí všechny nezbytné úkoly pro
     příjem dat k šifrování a připravení podklíčů pomocí nichž se data budou šifrovat.
     Probíhá tedy postupný zpracování dat přijatých z fp a následné jejich šifrování dokud
     je z čeho číst.

     funkce přijímá argumenty fp pro načítání dat z filu a key pomoci něhož se budou šifrovat data

     Vrací return code:
     OK (0) - vše proběhlo v pořádku
     FATAL_ERROR (2) - chyba při manipulaci s pamětí
 */
return_code encrypt(FILE *fp,  char *key){
    unsigned matrix_size = AES_128_KEY_LENGTH / 4;
    unsigned char **data_to_encrypt = NULL;
    unsigned char ***expanded_key = NULL;
    char buff[BUFFER_SIZE];

    return_code code;

    code = alloc_matrix_2d(&data_to_encrypt, matrix_size);

    if(code == FATAL_ERROR){
        return code;
    }

    code = key_expansion(&expanded_key, key, matrix_size);

    if(code == FATAL_ERROR){
        return code;
    }

    memset(buff, 0, BUFFER_SIZE);
    while(fread(buff, BUFFER_SIZE, 1, fp) == 1){
        fill_matrix(data_to_encrypt, buff, matrix_size);
        encrypt_data(data_to_encrypt, expanded_key, matrix_size);
        print_matrix_2d_as_line(data_to_encrypt, matrix_size);
        memset(buff, 0, BUFFER_SIZE);
    }

    fill_matrix(data_to_encrypt, buff, matrix_size);
    encrypt_data(data_to_encrypt, expanded_key, matrix_size);
    print_matrix_2d_as_line(data_to_encrypt, matrix_size);

    free_matrix_3d(&expanded_key, matrix_size, NUMBER_OF_ROUNDED_KEYS);
    free_matrix_2d(&data_to_encrypt, matrix_size);

    return code;
}



