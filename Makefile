CC = gcc
CFLAGS = -Wall -pedantic -ansi
BIN = aes.exe
OBJ = main.o encrypt.o matrix_lib.o  

%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@ 

$(BIN): $(OBJ)
	$(CC) $^ -o $@ 

