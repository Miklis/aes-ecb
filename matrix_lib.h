
#ifndef AEC_ECB_MATRIX_LIB_H
#define AEC_ECB_MATRIX_LIB_H

#include "return_code.h"

return_code alloc_matrix_2d(unsigned char ***out_matrix, unsigned width_and_height);

return_code alloc_matrix_3d(unsigned char ****out_matrix, unsigned width_and_height, unsigned depth);

void free_matrix_2d(unsigned char ***to_free_matrix, unsigned length);

void print_matrix_2d_as_line(unsigned char **matrix, unsigned length);

void free_matrix_3d(unsigned char ****to_free_matrix, unsigned length, unsigned depth);

void fill_matrix(unsigned char **matrix, char *data, unsigned length);

void print_matrix_2d(unsigned char **matrix, unsigned length);

void print_matrix_3d(unsigned char ***matrix, unsigned width_and_length, unsigned depth);

#endif
