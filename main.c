#include <stdio.h>
#include <string.h>
#include "return_code.h"
#include "encrypt.h"

#define ERR1 "ERR#1: Missing argument!\n"
#define ERR2 "ERR#2: Out of memory!\n"
#define ERR3 "ERR#3: Non-existent solution!\n"
#define ERR4 "ERR#4: Wrong key length!\n"
#define ERR5 "ERR#5: Some error occurs during data reading!\n"

/*
    Hlavní fuknce programu pro zpracování souboru pomocí algoritmu,
    kdy zajišťuje spouštění hlavních metod a ověření, že jsou všechny
    předané parametry programu správně zadané.

    Vrací return code:
    OK (0) - vše proběhlo v pořádku
    MISSING_ARGUMENT (1) - nebyl zadán jeden z parametrů nebo nebyl vyplněn
    WRONG_KEY_LENGTH (4) - Klíč má špatnou délku
    DATA_READ_ERROR (5) - Chyba při čtení dat
 */
return_code process(char *path, char *key){
    int key_len = strlen(key);
    FILE *fp;

    if(path == NULL || key == NULL){
        return MISSING_ARGUMENT;
    }

    if(key_len != AES_128_KEY_LENGTH){
        return WRONG_KEY_LENGTH;
    }

      fp = fopen(path, "r");

    if(fp != NULL){
        encrypt(fp, key);
    } else{
        return DATA_READ_ERROR;
    }

    fclose(fp);
    return OK;
}

void help(){

}

/*____________________________________________________________________________
  Funkce vypíše řešení řešené programem nebo příslušnou chybovou hlášku
  ____________________________________________________________________________
 */
void print_solution(return_code *result){
    switch (*result){
        case OK:
            break;
        case MISSING_ARGUMENT:
            printf(ERR1);
            help();
            break;
        case FATAL_ERROR:
            printf(ERR2);
            break;
        case NON_EXISTENT_SOLUTION:
            printf(ERR3);
            break;
        case WRONG_KEY_LENGTH:
            printf(ERR4);
            break;
        case DATA_READ_ERROR:
            printf(ERR5);
            break;
    }
}


int main(int argc, char **argv) {
    return_code result = MISSING_ARGUMENT;

    if(argc == 3){
        result = process(argv[1], argv[2]);
    }

    print_solution(&result);

    return result;

}
